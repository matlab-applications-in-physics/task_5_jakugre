%MATLAB 2020b
%name: resonance
%author: jakugre
%date: 2020-12-8
%version: v1.6
clear;

%Defining constants and values
r = 2.54/12/100; %radius of wire; [inch to m]
n = 40; %number of coils
m = 0.1; %mass of spring; [kg]
t_const = 10; %time constant; [s]
g = 9.80665; %gravitaional acceleration; [m/s^2]; source:https://physics.nist.gov/
G = 8e10; %Shear modulus; [Pa]
R = [0.005:0.002:0.050]; %Spring radius; [m]
f = [0:0.01:150]; %Frequency of used force; [Hz]

%Calculating beta, force and creating space to hold values of amplitudes
%for both different values of frequency and for different values of radius
beta = 1/(2*t_const);
F = m*g;
As = [];
As_1 = [];
fs =[];

%Calculating spring constant for all values of radius
k = (G*r^4)./(4*n*R.^3);
%Calculating omega of the spring
omega_0 = sqrt(k./m);
%Calculating omega of force's frequency
omega = 2*pi*f;

for j = 1:length(R)
    %Calculating amplitudes for each frequency
    for i = 1:length(f)
        x = (omega_0.^2 - omega(i)^2);
        A = (F/m)./(sqrt(x.^2 + 4*beta^2*omega_0.^2));
        As(i,:) = A;
    end
    %Recalculating spring constant and omega for each radius value
    k_1 = (G*r^4)/(4*n*R(j)^3);
    omega_0_R = sqrt(k_1/m);
    omega_R = sqrt(omega_0_R^2 + beta^2);
    
    %Calculating resonance frequency using equation:
    %2*π*f = (ω0^2+β^2)^(1/2)
    f_1 = omega_R/(2*pi);
    fs = [fs,f_1];
    
    %Calculating resonance amplitudes
    A_1 = F/(m*sqrt((omega_0_R^2 - omega_R^2)^2+4*beta^2*omega_R^2));
    As_1(j) = A_1;
end

%Creating plots
subplot(2,1,1);
plot1 = plot(f,As);
xlabel('Frequency of the force [Hz]');
ylabel('Amplitude of spring [m]');

subplot(2,1,2);
plot2 = plot(R,As_1);
xlabel('Radius of a coil [m]');
ylabel('Amplitude of spring [m]');

%Saving resonance data to file
file_format = "%s : %f, %s \n";
file = fopen('resonance_analysis_result.dat','w');
fprintf(file,file_format,'Wire radius',r,'[m]');
fprintf(file,file_format,'Number of coils',n,'[m]');
fprintf(file,file_format,'Mass of spring',m,'[kg]');
fprintf(file,file_format,'Time constant',t_const,'[s]');
fprintf(file,file_format,'Gravitational acceleration',g,'[m/s^2]');
fprintf(file,file_format,'Steel sheer modulus',G,'[Pa]');
fprintf(file,'Radius [m], Frequency [Hz], Amplitude [m] \n');

for k = [1:length(As_1)]
    fprintf(file,"%.3f, %.3f, %.2f \n",R(k),fs(k),As_1(k));
end



%Saving plots to file
pdf_name = 'resonance';
saveas(gcf,pdf_name,'pdf');
 
 



        
        
        
        
        
        
        


